const express = require('express');
const bodyPar = require('body-parser')
const asynExp = require('express-async-handler');
const corsExp = require('cors');
const app = express();
const port = 8800;

var pros = require('./model/index');

app.use(corsExp());
app.use(bodyPar.json());

app.get('/', (req, res) => {
    return res.send('prueba');
});

app.post('/users/', asynExp(async(req, res) => {
    var data = { 'user': req.body.user, 'pass': req.body.pass }
    var value = await pros.doPost(data);
    return res.send(value);
}));

app.get('/struct/:target', asynExp(async(req, res) => {
    var value = await pros.doGetStruct(req);
    return res.send(value);
}));

app.get('/functions/:target', asynExp(async(req, res) => {
    var value = await pros.doGetFunc(req);
    return res.send(value);
}));

app.get('/functions/:target/:id', asynExp(async(req, res) => {
    var value = await pros.doGetFunc(req);
    return res.send(value);
}));

app.put('/functions/:target', asynExp(async function(req, res) {
    var value = await pros.doPutPost(req);
    return res.send(value);
}));

app.post('/functions/:target', asynExp(async function(req, res) {
    var value = await pros.doPutPost(req);
    return res.send(value);
}));

app.delete('/functions/:target', asynExp(async function(req, res) {
    var value = await pros.doPutPost(req);
    return res.send(value);
}));

app.get('/outFunctions/:target', asynExp(async(req, res) => {
    var value = await pros.queryOut(req);
    return res.send({ data: value });
}));

app.listen(port, () =>
    console.log(`Example app listening on port ${port}!`),
);