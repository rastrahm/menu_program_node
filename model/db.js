var config = require('./config');
var promis = require('bluebird');

var options = {
    promiseLib: promis
};
var pgp = require('pg-promise')(options);

function conn() {
    var strConn = 'postgres://' + config.config.user + ':' + config.config.pass + '@' + config.config.host + ':' + config.config.port + '/' + config.config.data;
    const db = pgp(strConn);
    return db;
}

async function query(query) {
    if (typeof db == "undefined") {
        db = conn();
    }
    var data = await db.any(query);
    return data;
}
module.exports = { query };