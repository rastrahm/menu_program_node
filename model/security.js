function getSelect(data) {
    return "SELECT * FROM public.fnc_user_pass('" + data['user'] + "', public.encrypt('" + data['pass'] + "', 'prueba', 'bf'))";
}

function getVerify(data) {
    return "SELECT * FROM public.fnc_user_verify(" + data['id'] + ", '" + data['user'] + "')";
}

function getKey() {
    return "SELECT * FROM public.fnc_get_parameter('JWT_KEY')";
}

function getObject(profile, action) {
    if (action == 'menu') {
        return "SELECT * FROM public.fnc_previews_get_list()";
    } else if (action == 'profiles') {
        return "SELECT * FROM public.fnc_profile_get_all()";
    } else {
        return "SELECT * FROM public.fnc_men_obj(" + profile + ", '" + action + "')";
    }
}

function getMenu(profile) {
    return query = "SELECT * FROM public.fnc_men(" + profile + ")";
}

function getSubmenu(profile, menu) {
    return query = "SELECT * FROM public.fnc_men_sub(" + profile + ", " + menu + ")";
}

function getDictionary() {
    return "SELECT * FROM public.fnc_dictionary_get_all()";
}

function getTable(action) {
    return "SELECT * FROM public.fnc_table_descript('" + action + "')";
}

module.exports = { getSelect, getVerify, getKey, getObject, getMenu, getSubmenu, getDictionary, getTable };