var sc = require('./security');
var db = require('./db');

async function getMenus(profile) {
    var data = await db.query(sc.getMenu(profile));
    var objRet = { 'menus': [] };
    for (var i = 0; i < data.length; i++) {
        objRet.menus.push({
            'big_id': data[i]['big_id'],
            'str_menu': data[i]['str_menu'],
            'txt_description': data[i]['txt_description'],
            'str_submenu': await db.query(sc.getSubmenu(profile, data[i]['big_id']))
        })
    }
    return objRet;
}

module.exports = { getMenus }