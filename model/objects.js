function QueryConstruct(method, functions, fields, paranms, name) {
    var strFunction = '';
    if ((method == 'GET') && (name == paranms['menu'])) {
        strFunction = "SELECT * FROM public." + functions + '_' + method + '_ALL(';
        name = '';
    } else {
        strFunction = "SELECT * FROM public." + functions + '_' + method + '(';
    }
    var i = 0;
    if ((name) && ((paranms == '') || (paranms.length == 1))) {
        if (!isNaN(name)) {
            strFunction += name;
            i++;
        } else {
            strFunction += "'" + name + "'";
            i++;
        }
    }
    if (method == 'PUT') {
        for (var paranm in paranms) {
            if (paranm == 'big_id') {
                strFunction += paranms[paranm];
                i++;
            }
        }
    } else if (method == 'DELETE') {
        strFunction += "'" + paranms['delete'] + "'";
    }
    if (paranms) {
        if (typeof paranms === 'string' || paranms instanceof String) {
            var arrParanms = paranms.replace(/'/g, "").replace(/{/g, "").replace(/}/, "").split(',');
        } else {
            var arrParanms = paranms;
        }
        var strSeparator = "";
        var arrFields = fields.replace(/'/g, "").replace(/{/g, "").replace(/}/g, "").split(",");
        for (var h = 0; h < arrFields.length; h++) {
            if (i > 0) {
                strSeparator = ",";
            }
            for (var paranm in arrParanms) {
                if (arrFields[h] == paranm) {
                    if (!isNaN(arrParanms[paranm])) {
                        strFunction += strSeparator + "'" + arrParanms[paranm] + "'";
                    } else {
                        strFunction += strSeparator + "'" + arrParanms[paranm].replace(/&gt;/g, ">").replace(/&lt;/g, "<") + "'";
                    }
                }
            }
            i++;
        }
    }
    strFunction += ')';
    return strFunction;
}

module.exports = { QueryConstruct };