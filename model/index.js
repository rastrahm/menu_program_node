var db = require('./db');
var qy = require('./objects');
var sc = require('./security');
var me = require('./menus');
const jwt = require('jwt-simple');

//Texto de respuesta
async function queryOut(req) {
    var target = req.params.target;
    if (target == 'menu') {
        var data = await db.query(sc.getObject("0", target));
    } else {
        var data = await db.query(qy.QueryConstruct('GET', 'fnc_previews', '', '', target));
    }
    return data;
}

//Respuesta de intento de Login
async function doPost(data) {
    objSec = await db.query(sc.getSelect(data))
    if (objSec.length > 0) {
        var keyJWT = await getKey();
        var objPas = { 'id': objSec[0]['big_id'], 'user': data['user'], 'profile': objSec[0]['big_profile'] };
        var strJwt = await jwt.encode(objPas, keyJWT);
        var objMen = await me.getMenus(objSec[0]['big_profile']);
        var objDic = await db.query(sc.getDictionary());
        data = {
            'data': objSec,
            'jwt': strJwt,
            'menu': objMen,
            'dictionary': objDic
        };
    }
    return data;
}

//Funcion de obtención de los listados
async function doGetFunc(req) {
    var strJwt = req.get('jwt');
    var keyJWT = await getKey();
    var target = req.params.target;
    if (req.params.id == undefined) {
        if (strJwt != '') {
            var objJwt = await jwt.decode(strJwt, keyJWT);
            var data = { 'id': objJwt['id'], 'user': objJwt['user'] };
            var objSec = await db.query(sc.getVerify(data));
            if (objSec[0]['result']) {
                var strJwt = await jwt.encode(objJwt, keyJWT)
                if (target == 'profiles') {
                    data = await db.query(sc.getObject(objJwt['profile'], target));
                } else {
                    var params = { 'menu': target };
                    var objAct = await db.query(sc.getObject(objJwt['profile'], target));
                    data = await db.query(qy.QueryConstruct(req.method, objAct[0]['str_data_function'], '', params, target));
                }
                var data = { 'jwt': strJwt, 'table': target.substring(0, target.length - 1), 'data': data };
            }
        }
    } else {
        var getId = req.params.id;
        if (strJwt != '') {
            var objJwt = await jwt.decode(strJwt, keyJWT);
            var data = { 'id': objJwt['id'], 'user': objJwt['user'] };
            var objSec = await db.query(sc.getVerify(data));
            if (objSec[0]['result']) {
                var strJwt = await jwt.encode(objJwt, keyJWT)
                var params = '';
                var objAct = await db.query(sc.getObject(objJwt['profile'], target));
                data = await db.query(qy.QueryConstruct(req.method, objAct[0]['str_data_function'], '', params, getId));
                var data = { 'jwt': strJwt, 'table': target.substring(0, target.length - 1), 'data': data };
            }
        }
    }
    return data;
}

//Función de guardado
async function doPutPost(req) {
    var strJwt = req.get('jwt');
    var keyJWT = await getKey();
    var target = req.params.target;
    if (strJwt != '') {
        var objJwt = await jwt.decode(strJwt, keyJWT);
        var data = { 'id': objJwt['id'], 'user': objJwt['user'] };
        var objSec = await db.query(sc.getVerify(data));
        if (objSec[0]['result']) {
            var strJwt = await jwt.encode(objJwt, keyJWT)
            var params = req.body;
            var objAct = await db.query(sc.getObject(objJwt['profile'], target));
            data = await db.query(qy.QueryConstruct(req.method, objAct[0]['str_data_function'], objAct[0]['str_data_parameters'], params, ''));
            var data = { 'jwt': strJwt, 'table': target.substring(0, target.length - 1), 'data': data };
        }
    }
    return data;
}

async function doGetStruct(req) {
    var strJwt = req.get('jwt');
    var keyJWT = await getKey();
    var target = req.params.target;
    if (strJwt != '') {
        var objJwt = await jwt.decode(strJwt, keyJWT);
        var data = { 'id': objJwt['id'], 'user': objJwt['user'] };
        var objSec = await db.query(sc.getVerify(data));
        if (objSec[0]['result']) {
            var strJwt = await jwt.encode(objJwt, keyJWT);
            var objTab = await db.query(sc.getTable(target));
            var data = { 'jwt': strJwt, 'table': target, 'data': objTab };
        }
    }
    return data;
}

async function getKey() {
    var strSec = await db.query(sc.getKey());
    return strSec[0]['valor'];
}

module.exports = { queryOut, doPost, doGetFunc, getKey, doGetStruct, doPutPost };