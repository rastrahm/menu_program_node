class preview {
    var big_id = 0;
    var str_link = "";
    var txt_title = "";
    var txt_article = "";

    set big_id (big_id) {
        this.big_id = big_id;
    }

    get big_id () {
        return this.big_id;
    }

    set str_link (str_link) {
        this.str_link = str_link;
    }

    get str_link () {
        return this.str_link;
    }

    set txt_title (txt_title) {
        this.txt_title = txt_title;
    }

    get txt_title () {
        return this.txt_title;
    }

    set txt_article (txt_article) {
        this.txt_article = txt_article;
    }

    get txt_article () {
        return this.txt_article;
    }
}
